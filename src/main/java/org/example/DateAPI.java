package org.example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import java.time.LocalDateTime;

@SpringBootApplication
@RestController
public class DateAPI {

    public static void main(String[] args) {
        SpringApplication.run(DateAPI.class, args);
    }

    @GetMapping("/currentDate")
    public String getCurrentDate() {
        // Get current system date and time
        LocalDateTime currentDateTime = LocalDateTime.now();
        // Return the current date and time as a string
        return "Current Date and Time: " + currentDateTime;
    }
}
